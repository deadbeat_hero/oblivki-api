## Установка

``` composer require taroff/oblivki-api:* ```


**Реализовано всего 2 метода. Проект находится в разработке.**


## Пример использования


```
<?php

require 'vendor/autoload.php';
use taroff\oblivki\api\HeaderAuthMethod;
use taroff\oblivki\api\MethodWrapper;
use GuzzleHttp\Client;

$client = new Client([
    'base_uri' => 'https://api.oblivki.biz/1.0/',
    'timeout'  => 5.0,
]);

// в MethodWrapper набор методов апишки
$requester = new MethodWrapper($client, 
	new HeaderAuthMethod('XXXX')
);


$response = $requester->getLocation();
$locations = $response->getData();

//var_dump($locations);


foreach ($locations as $id => $location) {
	echo $location['id'], ') ', $location['title'], "\n";
	if (isset($location['childs'])) {
		foreach ($location['childs'] as $subLocation) {
			echo "	", $subLocation['id'], ') ', $subLocation['title'], "\n";
		}
	}
}



$response = $requester->getBrowser();
$items = $response->getData();

var_dump($items);


foreach ($items as $id => $item) {
	echo $item['id'], ') ', $item['name'], "\n";
}



$response = $requester->getPlatform();
$items = $response->getData();


foreach ($items as $id => $item) {
	echo $item['id'], ') ', $item['name'], "\n";
}



// CREATE
define('IMG_PATH', '/home/taroff/Pictures/teaser/');
define('CAMPAIGN_ID', 74346);
$images = [];
foreach (glob(IMG_PATH . "*.jpg") as $filename) {
	$images[] = $filename;
}

$teasers = [];
foreach ($images as $image) {
	$teasers[] = [
	    'campaignId' => CAMPAIGN_ID,
    	'text' => 'Teaser test text ' . mt_rand() . ' with long names too, longlong text here',
    	'uploaded' => base64_encode(file_get_contents($image)),
    	'url' => 'http://ya.ru',
    	'bid' => 0.67,
	];
}

echo 'found: ', count($teasers), "\n";

foreach ($teasers as $teaser) {
	$response = $requester->teaserCreate($teaser);
	var_dump($response);
}


// START STOP DEWLETE
//1021237
$response = $requester->teaserSetStatus(1021235, 'stop');
var_dump($response);


$response = $requester->teaserSetStatus(1021243, 'delete');
var_dump($response);


echo "LIMIT TEST\n";
$iCount = 0;
while (true) {
	$response = $requester->campaigns();
	if ($response->code != 200) {
		break;
	}

	$iCount++;
}
echo $iCount, "\n";
var_dump($response);
echo "END LIMIT TEST\n\n";

echo "campaignById\n";
$response = $requester->campaignById(74346);
var_dump($response);
echo "END campaignById\n\n";


echo "teasers\n";
$response = $requester->teasers(null, 'moderation');
var_dump($response);
echo "END teasers\n\n\n";


$response = $requester->teaserById(1021235);
var_dump($response);



$response = $requester->teaserUpdate([
	'id' => 1021243,
	'bid' => 15,
	//'text' => '',
]);

var_dump($response);

$response = $requester->campaignCreate([
	'id' => 1021243,
	'bid' => 15,
	'typeId' => 1,
	'location' => 1,
	'name' => 'Через API II'
	//'text' => '',
]);

var_dump($response);

```