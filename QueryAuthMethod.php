<?php

namespace taroff\oblivki\api;
use GuzzleHttp\Psr7\Request;

class QueryAuthMethod implements AuthMethodInterface
{
	protected $username;
	protected $password;
	protected $token;
	protected $httpClient;

	public function __construct(\GuzzleHttp\Client $httpClient, $username, $password)
	{
		$this->username = $username;
		$this->password = $password;
		$this->httpClient = $httpClient;
	}

	protected function createToken()
	{
		$response = $this->httpClient->request('POST', 'auth/login', [
			'form_params' =>[
				'login' => $this->username, 
				'password' => $this->password
			]
		]);

		$responseText = $response->getBody()->getContents();
		$data = json_decode($responseText, true);
		if (isset($data['access-token'])) {
			$this->token = $data['access-token'];
		} else {
			throw new \Exception("Can't get access-token: " . $responseText, $response->getStatusCode());
		}
	}

	public function getToken()
	{
		if (null === $this->token) {
			$this->createToken();
		}

		return $this->token;
	}

	public function addAuth(array &$getParams, array &$postParams, array &$headers)
	{
		$getParams['access-token'] = $this->getToken();
	}
}
