<?php

namespace taroff\oblivki\api;

use GuzzleHttp\Psr7\Request;

interface AuthMethodInterface
{
	public function addAuth(array &$getParams, array &$postParams, array &$headers);
}
