<?php

namespace taroff\oblivki\api;

class SimpleResponse
{
	public $code;
	public $data;

	public function __construct($code, $bodyText)
	{
		$this->code = $code;
		$this->data = json_decode($bodyText, true);
	}

	public function getParam($name, $default = null)
	{
		return isset($this->data[$name]) ? $this->data[$name] : $default;
	}

	public function getCode()
	{
		return $this->code;
	}

	public function getData()
	{
		return $this->data;
	}
}
