<?php

namespace taroff\oblivki\api;
use GuzzleHttp\Psr7\Request;

class HeaderAuthMethod implements AuthMethodInterface
{
	protected $token;

	public function __construct($token)
	{
		$this->token = $token;
	}

	public function getToken()
	{
		return $this->token;
	}

	public function addAuth(array &$getParams, array &$postParams, array &$headers)
	{
		$headers['Authorization'] = 'access-token ' . $this->getToken();
	}
}
