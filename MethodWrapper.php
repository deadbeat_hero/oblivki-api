<?php

namespace taroff\oblivki\api;

use GuzzleHttp\Psr7\Request;

class MethodWrapper
{
	public $auth;
	public $client;

	protected $query = [];
	protected $formParams = [];
	protected $headers = [];

	public function __construct(\GuzzleHttp\Client $client, AuthMethodInterface $auth)
	{
		$this->auth = $auth;
		$this->client = $client;
	}

	protected function addQueryParam($name, $value)
	{
		$this->query[$name] = $value;
	}

	protected function addAuth()
	{
		$this->auth->addAuth($this->query, $this->formParams, $this->headers);
	}

	protected function clearQuery()
	{
		$this->query = [];
	}

	public function makeReturn($request)
	{
		$this->addAuth($request);

		$opts = [];
		if ($this->query) {
			$opts['query'] = $this->query;
		}

		if ($this->formParams) {
			$opts['form_params'] = $this->formParams;
		}

		if ($this->headers) {
			$opts['headers'] = $this->headers;
		}

		try {
			$response = $this->client->send($request, $opts);
			
		} catch (\Exception $e) {
			$response = $e->getResponse();
			if (null === $response) {
				throw $e;
			}
		}


		return new SimpleResponse($response->getStatusCode(), $response->getBody()->getContents());
	}

	/**
	*	https://oblivki.biz/api-doc#userbalance
	*/
	public function userBalance()
	{
		$this->clearQuery();
		$request = new Request('GET', 'user/balance');
		return $this->makeReturn($request);
	}

	public function setFormParams($params)
	{
		$this->formParams = $params;
	}

	public function campaignCreate($data)
	{
		$this->clearQuery();
		$this->setFormParams($data);
		$request = new Request('POST', 'campaign/create');
		return $this->makeReturn($request);
	}

	public function campaignUpdate($data)
	{
		$this->clearQuery();
		$this->setFormParams($data);
		$request = new Request('POST', 'campaign/update');
		return $this->makeReturn($request);
	}

	/**
	*	https://oblivki.biz/api-doc#teasercreate
	*/
	public function teaserCreate($data)
	{
		$this->clearQuery();
		$this->setFormParams($data);
		$request = new Request('POST', 'teaser/create');
		return $this->makeReturn($request);
	}

	public function teaserUpdate($data)
	{
		$this->clearQuery();
		$this->setFormParams($data);
		$request = new Request('POST', 'teaser/update');
		return $this->makeReturn($request);
	}

	public function teaserSetStatus($id, $status)
	{
		$this->clearQuery();
		$this->setFormParams(['id' => $id]);
		$request = new Request('POST', 'teaser/' . $status);
		return $this->makeReturn($request);
	}

	public function getBrowser()
	{
		$this->clearQuery();
		$request = new Request('GET', 'browsers');
		return $this->makeReturn($request);
	}

	public function getLocation()
	{
		$this->clearQuery();
		$request = new Request('GET', 'locations');
		return $this->makeReturn($request);
	}

	public function getPlatform()
	{
		$this->clearQuery();
		$request = new Request('GET', 'platforms');
		return $this->makeReturn($request);
	}

	public function campaigns()
	{
		$this->clearQuery();
		$request = new Request('GET', 'campaigns');
		return $this->makeReturn($request);
	}

	public function campaignById($id)
	{
		$this->clearQuery();
		$request = new Request('GET', 'campaign/' . $id);
		return $this->makeReturn($request);
	}

	public function teasers($campaignId = null, $status = null)
	{
		$this->clearQuery();
		
		if ($campaignId) {
			$this->addQueryParam('campaignId', $campaignId);
		}

		if ($status) {
			$this->addQueryParam('status', $status);
		}

		$request = new Request('GET', 'teasers');
		return $this->makeReturn($request);
	}

	public function teaserById($id)
	{
		$this->clearQuery();
		$request = new Request('GET', 'teaser/' . $id);
		return $this->makeReturn($request);
	}
}
